package com.titan.worker.amazonServices;

import com.titan.worker.response.model.audio.AudioItem;
import com.titan.worker.response.model.audio.PlayList;
import com.titan.worker.response.model.users.User;
import com.titan.worker.response.model.users.Users;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.concurrent.ThreadLocalRandom;
import static org.jooq.util.Tables.AUDIODATA;
import static org.jooq.util.Tables.USERS;

/**
 * Created by ascelhem on 7/18/16.
 */
public class PlayListWorker implements Runnable{

    private Connection connection = null;
    private String userName = "root";
    private String password = "ws56z7f3";
    private String url = "jdbc:postgresql://meta-data-storage.ciyc9sxx9jim.us-west-1.rds.amazonaws.com:6000/metadata";
    private PlayList playList=null;

    public PlayListWorker(PlayList _playlist){
        this.playList = _playlist;
    }
    @Override
    public void run() {
        String name = Thread.currentThread().getName();
        ThreadLocalRandom random = ThreadLocalRandom.current();
        try (Connection conn = DriverManager.getConnection(url, userName, password)) {
            DSLContext create = DSL.using(conn, SQLDialect.POSTGRES_9_4);
            System.out.println("[INFO] Saving playlist, USER ID: " + playList.getUser_id());
            for(AudioItem item : playList.getAudioItemList()){
                create.insertInto(AUDIODATA,
                        AUDIODATA.USER_ID, AUDIODATA.ARTIST_NAME, AUDIODATA.SONG_NAME, AUDIODATA.AUDIO_ID)
                        .values((long)playList.getUser_id(), item.getArtist(), item.getTitle(), (long)item.getId()).execute();
            }
            System.out.println("[INFO] Update user's flag");
                create.update(USERS).set(USERS.AUDIO_FETCH, 2).execute();
            System.out.println("[INFO] User saved");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}