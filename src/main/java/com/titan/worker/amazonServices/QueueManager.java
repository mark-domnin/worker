package com.titan.worker.amazonServices;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.titan.worker.response.model.audio.PlayList;
import com.titan.worker.response.model.users.Users;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class QueueManager<T>{
    private ExecutorService service = null;
    private QueueManager(){
        service = Executors.newFixedThreadPool(6);
    }

    private static class QueueManagerHolder{
        private static final QueueManager INSTANCE = new QueueManager();
    }
    public static QueueManager getInstance(){
        return QueueManagerHolder.INSTANCE;
    }

    public void push(T object){
        try{
            if(object instanceof Users){
                service.submit(new UserWorker((Users) object));
            }else if(object instanceof PlayList){
                service.submit(new PlayListWorker((PlayList) object));
            }else{
                System.out.println("[INFO] Unknown type of object");
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        /*Gson gson = new GsonBuilder()
                .registerTypeAdapter(Playlist.class, new ResponseDeserializer<Playlist>())
                .registerTypeAdapter(Users.class, new ResponseDeserializer<Users>())
                .registerTypeAdapter(MembersCount.class, new ResponseDeserializer<MembersCount>())
                .registerTypeAdapter(PlayLists.class, new ResponseDeserializer<Playlist>())
                .disableHtmlEscaping()
                .create();
        System.out.println(json);
        try{
            try{
                JsonObject e = json.getAsJsonObject().get("response").getAsJsonObject().get("playlists").getAsJsonObject();
                service.submit(new PlayListWorker(new Gson().fromJson(json, PlayLists.class)));
            }catch (Exception e){
                e.printStackTrace();
                System.out.println("[INFO] Playlist received;");
            }
                service.submit(new UserWorker(new Gson().fromJson(json, Users.class)));

        }catch (Exception e){
            e.printStackTrace();
        }*/
    }
    public void shutdown(){
        service.shutdown();
        System.out.println("Awaiting termination...");
        try {
            service.awaitTermination(1000, TimeUnit.HOURS);
        } catch (InterruptedException e) {
            System.err.println("Failed to terminate worker");
        }
    }
}
