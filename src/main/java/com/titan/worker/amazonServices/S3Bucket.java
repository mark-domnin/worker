package com.titan.worker.amazonServices;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;

public class S3Bucket {
    private AWSCredentials credentials;
    private AmazonS3 s3;
    private String s3BucketName = "queue-manager";


    public S3Bucket(AWSCredentials _credentials){
        credentials = _credentials;
        s3 = new AmazonS3Client(credentials);
        s3.setRegion(Region.getRegion(Regions.US_WEST_2));
    }
    public AmazonS3 getAmazonS3(){
        return this.s3;
    }
    public String getAmazonS3Name(){
        return this.s3BucketName;
    }
}
