package com.titan.worker.amazonServices;

import com.titan.worker.response.model.users.User;
import com.titan.worker.response.model.users.Users;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.concurrent.ThreadLocalRandom;
import static org.jooq.util.Tables.USERS;






/**
 * Created by ascelhem on 7/18/16.
 */
public class UserWorker implements Runnable{

    private Connection connection = null;
    private String userName = "root";
    private String password = "ws56z7f3";
    private String url = "jdbc:postgresql://meta-data-storage.ciyc9sxx9jim.us-west-1.rds.amazonaws.com:6000/metadata";
    private Users users=null;

    public UserWorker(Users _users){
        this.users = _users;
    }
    @Override
    public void run() {
        String name = Thread.currentThread().getName();
        ThreadLocalRandom random = ThreadLocalRandom.current();
        try (Connection conn = DriverManager.getConnection(url, userName, password)) {
            DSLContext create = DSL.using(conn, SQLDialect.POSTGRES_9_4);
            for(User user : users.getUsers()){
                System.out.println("[INFO] Saving user ID: " + user.getId());
                if(user.getCan_see_audio() != 0){
                    create.insertInto(USERS,
                            USERS.USER_ID,
                            USERS.CITY_NAME,
                            USERS.CITY_ID,
                            USERS.AUDIO_FETCH)
                            .values(user.getId(), user.getCity().getTitle(), user.getCity().getId(), 0).execute();
                    System.out.println("[INFO] User passed to Database");
                }else{
                    System.out.println("[INFO] Unable to reach audio, User ID: " + user.getId() + ". Skip");
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}