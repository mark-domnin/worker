package com.titan.worker.amazonServices;

import com.amazon.sqs.javamessaging.AmazonSQSExtendedClient;
import com.amazon.sqs.javamessaging.ExtendedClientConfiguration;
import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.BucketLifecycleConfiguration;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.List;
import java.util.UUID;

public class SQSExtended{
    //TODO: FIX THIS FUCKING CLASS. ITS UNGLY;
    private AWSCredentials credentials = null;
    private String s3BucketName = "queue-manager";
    private AmazonSQS sqsExtended;
    private String myQueueUrl;
    public SQSExtended(){
        try{
            try {
                credentials = new ProfileCredentialsProvider("default").getCredentials();
            } catch (Exception e) {
                throw new AmazonClientException(
                        "Cannot load the AWS credentials from the expected AWS credential profiles file. "
                                + "Make sure that your credentials file is at the correct "
                                + "location (/home/$USER/.aws/credentials) and is in a valid format.", e);
            }
        }catch (Exception e){
            e.printStackTrace();
            System.exit(0);
        }
        AmazonS3 s3 = new AmazonS3Client(credentials);
        Region s3Region = Region.getRegion(Regions.US_WEST_2);
        s3.setRegion(s3Region);

        BucketLifecycleConfiguration.Rule expirationRule = new BucketLifecycleConfiguration.Rule();
        expirationRule.withExpirationInDays(14).withStatus("Enabled");
        BucketLifecycleConfiguration lifecycleConfig = new BucketLifecycleConfiguration().withRules(expirationRule);

        s3.setBucketLifecycleConfiguration("queue-manager", lifecycleConfig);
        System.out.println("[INFO] Bucket engaged.");

        ExtendedClientConfiguration extendedClientConfig = new ExtendedClientConfiguration()
                .withLargePayloadSupportEnabled(s3, s3BucketName);

        sqsExtended = new AmazonSQSExtendedClient(new AmazonSQSClient(credentials), extendedClientConfig);
        Region sqsRegion = Region.getRegion(Regions.US_WEST_2);
        sqsExtended.setRegion(sqsRegion);

        // Create a message queue for this example.
        String QueueName = UUID.randomUUID() + "-"
                + DateTimeFormat.forPattern("yyMMdd-hhmmss").print(new DateTime());

        CreateQueueRequest createQueueRequest = new CreateQueueRequest(QueueName);
        myQueueUrl = sqsExtended.createQueue(createQueueRequest).getQueueUrl();
        System.out.println("[INFO] Queue created.");
        System.out.println(myQueueUrl);


    }
    //get message from queue
    public List<Message> get(){
        ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(myQueueUrl);
        return sqsExtended.receiveMessage(receiveMessageRequest).getMessages();
    }
    //delete message from queue
    public void delete(List<Message> messages){
        for(Message messageReceiptHandle: messages){
            String handle = messageReceiptHandle.getReceiptHandle();
            sqsExtended.deleteMessage(myQueueUrl, handle);
        }
        System.out.println("[INFO] Message was deletes");
    }
}
