package com.titan.worker.response.model.users;

/**
 * Created by ascelhem on 7/17/16.
 */
public class City {
    private Integer id=null;
    private String title=null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
