package com.titan.worker.response.model.response;

import com.google.gson.*;

import java.lang.reflect.Type;

/**
 * Created by ascelhem on 7/17/16.
 */
public class ResponseDeserialize implements JsonDeserializer {
    @Override
    public Response deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        JsonObject response = jsonElement.getAsJsonObject();
        Response r = new Response();
        if(response.has("error")){
            return new Gson().fromJson(response, Response.class);
        }else{
            if(response.has("type")){
                r.setType(response.get("type").getAsString());
            }
        }
        return r;
    }

}
