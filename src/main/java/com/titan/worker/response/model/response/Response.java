package com.titan.worker.response.model.response;


import java.lang.*;

/**
 * Created by ascelhem on 7/17/16.
 */
public class Response {
    private String response = null;
    private Error error = null;
    private String type=null;

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
