package com.titan.worker.response.model.gson;

import com.titan.worker.response.model.group.Group;
import com.google.gson.*;

import java.lang.reflect.Type;

/**
 * Created by ascelhem on 7/18/16.
 */
public class GroupDeserialize implements JsonDeserializer{
    @Override
    public Object deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        JsonObject jo = jsonElement.getAsJsonObject().get("response").getAsJsonArray().get(0).getAsJsonObject();
        return new Gson().fromJson(jo, Group.class);
    }
}
