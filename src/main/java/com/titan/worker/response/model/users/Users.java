package com.titan.worker.response.model.users;

import com.titan.worker.response.model.users.User;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by ascelhem on 7/17/16.
 */
public class Users {

    private List<User> users = new LinkedList<>();

    public List<User> getUsers(){
        return new LinkedList<>(users);
    }
    public void addUser(User user)
    {
        this.users.add(user);
    }

    private String type="users";
    private String error = null;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}