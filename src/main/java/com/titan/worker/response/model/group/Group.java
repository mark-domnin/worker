package com.titan.worker.response.model.group;

/**
 * Created by ascelhem on 7/18/16.
 */
public class Group {
    private int id;
    private int members_count;

    public int getMembers_count() {
        return members_count;
    }

    public void setMembers_count(int members_count) {
        this.members_count = members_count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
