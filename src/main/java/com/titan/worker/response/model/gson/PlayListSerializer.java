package com.titan.worker.response.model.gson;

import com.titan.worker.response.model.audio.PlayList;
import com.titan.worker.response.model.users.Users;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

/**
 * Created by ascelhem on 7/17/16.
 */
public class PlayListSerializer implements JsonSerializer<PlayList>
{
    @Override
    public JsonElement serialize(PlayList src, Type typeOfSrc, JsonSerializationContext context)
    {
        JsonObject items = new JsonObject();
        JsonObject response = new JsonObject();


        items.add("items", context.serialize(src.getAudioItemList()));
        response.add("response", items);
        response.add("type", context.serialize(src.getType()));
        response.addProperty("user_id", src.getUser_id());

        return response;
    }
}