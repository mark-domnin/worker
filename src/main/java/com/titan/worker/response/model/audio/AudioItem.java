package com.titan.worker.response.model.audio;

/**
 * Created by ascelhem on 7/18/16.
 */
public class AudioItem {
    private int id;
    private String artist;
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
