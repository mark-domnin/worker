package com.titan.worker.response.model.gson;

import com.titan.worker.response.model.users.User;
import com.titan.worker.response.model.users.Users;
import com.google.gson.*;

import java.lang.reflect.Type;

/**
 * Created by ascelhem on 7/17/16.
 */
public class UsersDeserialize implements JsonDeserializer {
    @Override
    public Object deserialize(JsonElement jsonObject, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        Users users = new Users();
        try{
            if(!jsonObject.getAsJsonObject().has("error")){
                JsonArray items = jsonObject.getAsJsonObject()
                        .get("response").getAsJsonObject()
                        .get("items").getAsJsonArray();
                for(JsonElement e : items){
                    users.addUser((User) jsonDeserializationContext.deserialize(e, User.class));
                }
            }else{
                users.setError(jsonObject.getAsJsonObject().get("error").getAsJsonObject().get("error_code").getAsString());
            }


        }catch (Exception e){
            System.out.println("[ERROR] An error has accused while parsing the response.");
            System.out.println("[ERROR] DataModel: " + jsonObject);
            e.printStackTrace();
        }

        return users;
    }
}
