package com.titan.worker.response.model.response;

/**
 * Created by ascelhem on 7/17/16.
 */
public class Error {
    private Integer error_code =null;
    private String error_msg = null;

    public Integer getError_code() {
        return error_code;
    }

    public void setError_code(Integer error_code) {
        this.error_code = error_code;
    }

    public String getError_msg() {
        return error_msg;
    }

    public void setError_msg(String error_msg) {
        this.error_msg = error_msg;
    }
}
