package com.titan.worker.response.model.audio;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by ascelhem on 7/18/16.
 */
public class PlayList {
    private int user_id;
    private List<AudioItem> audioItemList = new LinkedList<>();
    private String type="playlist";
    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public List<AudioItem> getAudioItemList() {
        return audioItemList;
    }

    public void add(AudioItem ai) {
        this.audioItemList.add(ai);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
