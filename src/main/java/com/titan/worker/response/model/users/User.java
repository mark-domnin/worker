package com.titan.worker.response.model.users;

import com.titan.worker.response.model.users.City;

/**
 * Created by ascelhem on 7/17/16.
 */
public class User {
    public User(){}

    private int id;
    private String first_name;
    private String last_name;
    private int can_see_audio;
    private City city=null;


    public void setId(int _id){
        this.id = _id;
    }
    public int getId(){
        return this.id;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public int getCan_see_audio() {
        return can_see_audio;
    }

    public void setCan_see_audio(int can_see_audio) {
        this.can_see_audio = can_see_audio;
    }
}
