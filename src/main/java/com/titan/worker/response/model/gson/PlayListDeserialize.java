package com.titan.worker.response.model.gson;

import com.titan.worker.response.model.audio.AudioItem;
import com.titan.worker.response.model.audio.PlayList;
import com.titan.worker.response.model.users.User;
import com.titan.worker.response.model.users.Users;
import com.google.gson.*;

import javax.print.attribute.standard.JobSheets;
import java.lang.reflect.Type;

/**
 * Created by ascelhem on 7/18/16.
 */
public class PlayListDeserialize implements JsonDeserializer {
    @Override
    public Object deserialize(JsonElement jsonObject, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        PlayList playList = new PlayList();
        try{
            JsonObject jo = jsonObject.getAsJsonObject();
            playList.setUser_id(jo.get("user_id").getAsInt());
            JsonArray items = jo.get("response").getAsJsonObject().get("items").getAsJsonArray();
            for(JsonElement e : items){
                playList.add((AudioItem) jsonDeserializationContext.deserialize(e, AudioItem.class));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return playList;
    }
}