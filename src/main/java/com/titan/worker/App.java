package com.titan.worker;

import com.amazonaws.services.sqs.model.Message;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.titan.worker.amazonServices.QueueManager;
import com.titan.worker.amazonServices.SQSExtended;
import com.titan.worker.response.model.audio.PlayList;
import com.titan.worker.response.model.group.Group;
import com.titan.worker.response.model.gson.*;
import com.titan.worker.response.model.response.Response;
import com.titan.worker.response.model.response.ResponseDeserialize;
import com.titan.worker.response.model.users.Users;

import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{

    public static void main( String[] args )
    {

        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Response.class, new ResponseDeserialize())
                .registerTypeAdapter(Users.class, new UsersSerializer())
                .registerTypeAdapter(Users.class, new UsersDeserialize())
                .registerTypeAdapter(PlayList.class, new PlayListDeserialize())
                .registerTypeAdapter(PlayList.class, new PlayListSerializer())
                .registerTypeAdapter(Group.class, new GroupDeserialize())
                .create();


        SQSExtended queue = new SQSExtended();
        QueueManager manager = QueueManager.getInstance();
        while (true){
            try{
                List<Message> messages = queue.get();
                if (messages.size() > 0){
                    System.out.println("[INFO] Incoming message...");
                    for(Message message : messages){

                        Response r = gson.fromJson(message.getBody(), Response.class);
                        if (r.getError() == null){
                            switch (r.getType()){
                                case "playlist":
                                        System.out.println("[INFO] Playlist detected. Starting...");
                                        PlayList pl = gson.fromJson(message.getBody(), PlayList.class);
                                        manager.push(pl);
                                        System.out.println("[INFO] Task passed to queue");
                                    break;
                                case "users":
                                        System.out.println("[INFO] Users detected. Starting...");
                                        Users u = gson.fromJson(message.getBody(), Users.class);
                                        manager.push(u);
                                        System.out.println("[INFO] Task passed to queue");
                                    break;
                                default:
                                    throw new RuntimeException("");
                            }
                        }else {
                            System.out.println("[ERROR] Message contains an error. Skip;");
                            System.out.println("[ERROR] Message ID: " +message.getMessageId());
                            System.out.println("[ERROR] Message Body: " + message.getBody());
                            queue.delete(messages);
                        }
                    }
                }else{
                    System.out.println(System.currentTimeMillis()+" [INFO] Waintng for job...");
                    Thread.sleep(5000);
                }
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }
}
